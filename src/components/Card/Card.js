import React from 'react';
import './Card.css'
import Box from '../../assets/images/Group.png'

const Card = (props) => {
    const title = props.title;
    const price = props.price;
    const gender = props.gender; 
    const age = props.age;
    const image = props.image;
    const product = props.product;

    return (
        <>
            {product && product === 'info' ?
                <div className='wrap-card wrap-card-info'>
                    <img  className="logo" src={image} alt="img" />
                    <div className='knowledge'>{title}</div>
                    <div className='title'>{price}</div>
                    <div className='wrap-gender-age'>
                        <div className='gender'>{gender}</div>
                    </div>
                </div>
            :
                <div className='wrap-card'>
                    <img  className="logo" src={image} alt="img" />
                    <div className='title'>{title}</div>
                    <div className='wrap-gender-age'>
                        <div className='gender'>{product ?'Product:' : 'Gene:'} <span>{gender}</span></div>
                        <div className='dot'></div>
                        <div className='age'>{product ?'Size:' : 'Age:'}  <span>{age}</span></div>
                    </div>
                    <div className='price'>{price}</div>
                    {product ?
                        <div className='product-add'>
                            <img  className="box" src={Box} alt="img" />
                            <div className='dot'></div>
                            <div className='text-product'>Free Toy & Free Shaker</div>
                        </div>
                    :null}
                </div>
            }
            
        </>
    );
};

export default Card;
