import React from 'react';
import './Footer.css'
import Sosmed from '../../assets/images/Frame 25.png';
import Logo from '../../assets/images/Frame.png';

const Footer = () => {
    return (
        <div className='wrap-footer'>
            <div className='footer-area'>
                <div className='top-area'>
                    <h4>Register now so you don't miss our programs</h4>
                    <form className='wrap-search'>
                        <input
                            type="text"
                            placeholder="Enter your Email"
                        />
                        <button>Subcribe Now</button>
                    </form>
                </div>
                <div className='center-area'>
                    <div className='footer-menu'>
                        <p>Home</p>
                        <p>Category</p>
                        <p>About</p>
                        <p>Contact</p>
                    </div>
                    <img className='sosmed-area' src={Sosmed} alt={Sosmed} />
                </div>
                <div className='bottom-area'>
                    <p className='copy-right'>© 2022 Monito. All rights reserved.</p>
                    <div className='wrap-Logo'>
                        <img className='Logo-area' src={Logo} alt={Logo} />
                    </div>
                    <div className='wrap-policy'>
                        <p className='policy'>Terms of Service</p>
                        <p className='policy'>Privacy Policy</p>
                    </div>
                    <p className='copy-right mobile-only'>© 2022 Monito. All rights reserved.</p>
                </div>
            </div>
        </div>
    );
};

export default Footer;
