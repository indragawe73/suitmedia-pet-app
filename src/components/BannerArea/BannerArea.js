import React from 'react';
import './BannerArea.css'
import ButtonMain from '../ButtonMain/ButtonMain'

const BannerArea = (props) => {
    const type = props.type;
    const title = props.title;
    const text = props.text;
    const description = props.description;

    return (
        <>
            {type === 'left' ?
                <div className='text-banner'>
                    <h1>{title}</h1>
                    <h3>{text}</h3>
                    <p>{description}</p>
                    <div className='wrap-btn'>
                        <ButtonMain text="View Intro" level="one" />
                        <ButtonMain text="Explore Now" level="two" />
                    </div>
                </div>
            : type === 'leftOn' ?
                <div className='text-banner text-banner-leftOn'>
                    <h1>{title}</h1>
                    <h3>{text}</h3>
                    <p>{description}</p>
                    <div className='wrap-btn'>
                        <ButtonMain text="View Intro" level="one" />
                        <ButtonMain text="Explore Now" level="two" />
                    </div>
                </div>
            : 
                <div className='text-banner text-banner-right'>
                    <h1>{title}</h1>
                    <h3>{text}</h3>
                    <p>{description}</p>
                    <div className='wrap-btn'>
                        <ButtonMain text="View Intro" level="one" />
                        <ButtonMain text="Explore Now" level="two" />
                    </div>
                </div>
            }
        </>
    );
};

export default BannerArea;
