import React from 'react';
import './HeaderArea.css'
import Menu from '../../assets/images/Vector.png';
import Logo from '../../assets/images/Frame.png';
import SearchMobile from '../../assets/images/Vector 2.png';
import Search from '../../assets/images/u_search.png';
import Flag from '../../assets/images/Frame 45.png';
import Arrow from '../../assets/images/Caret_Down_SM.png';

const HeaderArea = () => {
    return (
        <div className='wrap-header'>
            <div className='header-left'>
                <img  className="logo-mobile" src={Menu} alt="Menu" />
                <img  className="logo" src={Logo} alt="logo" />
                <img  className="logo-mobile" src={SearchMobile} alt="SearchMobile" />
                <p className='header-menu'>Home</p>
                <p className='header-menu'>Category</p>
                <p className='header-menu'>About</p>
                <p className='header-menu'>Contact</p>
            </div>
            <div className='header-right'>
                <form className='search-area'>
                    <img  className="logo" src={Search} alt="Search" />
                    <input
                        type="text"
                        placeholder="Search something here!"
                    />
                </form>
                <button className='btn-join'>Join the community</button>
                <div className='wrap-flag'>
                    <img  className="logo" src={Flag} alt="Flag" />
                    <div className='flag-text'>vnd</div>
                    <img  className="logo" src={Arrow} alt="Arrow" />
                </div>

            </div>
        </div>
    );
};

export default HeaderArea;
