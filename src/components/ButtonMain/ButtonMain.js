import React from 'react';
import './ButtonMain.css'
import Play from '../../assets/images/Play_Circle.png';
import Arrow from '../../assets/images/Chevron_Right_MD.png';

const ButtonMain = (props) => {
    const level = props.level;
    const text = props.text;

    return (
        <>
            {level === 'one'? 
                <button className='btn-area'>
                    <p className='btn-text'>{text}</p>
                    <img  className="logo" src={Play} alt="Play" />
                </button>
            : level === 'two'?
                <button className='btn-area-two'>
                    <p className='btn-text'>{text}</p>
                </button>
            :
                <button className='btn-area btn-area-tree'>
                    <p className='btn-text'>{text}</p>
                    <img  className="logo" src={Arrow} alt="Arrow" />
                </button>
            }
        </>
    );
};

export default ButtonMain;
