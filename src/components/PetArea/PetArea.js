import React from 'react';
import './PetArea.css'
import ButtonMain from '../ButtonMain/ButtonMain'
import Card from '../Card/Card'
import Arrow from '../../assets/images/Chevron_Right_MD.png';

import { listPets, listProducts, listInfo } from '../../utils/DataList'

const PetArea = (props) => {
    const title = props.title;
    const text = props.text;
    const product = props.product;

    return (
        <div className='wrap-pet'>
            <div className='title-area'>
                <div className='title-text'>
                    <p>{title}</p>
                    <h3>{text}</h3>
                </div>
                <ButtonMain text='View more' level='tree'/>
            </div>
            <div className='card-pet-area'>
                {product === 'product' ?
                    <>
                        {listProducts.map((item, idx) => (
                            <Card key={idx} image={item.img} title={item.title} price={item.price} gender={item.gender} age={item.age} product={product} />
                        ))}
                    </>
                    : product === 'info' ?
                    <>
                        {listInfo.map((item, idx) => (
                            <Card key={idx} image={item.img} title={item.title} price={item.price} gender={item.gender} age={item.age} product={product} />
                        ))}
                    </>
                    : 
                    <>
                        {listPets.map((item, idx) => (
                            <Card key={idx} image={item.img} title={item.title} price={item.price} gender={item.gender} age={item.age} />
                        ))}
                    </>
                }
            </div>
            
            <button className='btn-area btn-area-tree btn-mobile'>
                <p className='btn-text'>View more</p>
                <img  className="logo" src={Arrow} alt="Arrow" />
            </button>
        </div>
    );
};

export default PetArea;
