import React from 'react';
import './SmallBanner.css';
import BannerArea from '../BannerArea/BannerArea';
import handOn from '../../assets/images/pngegg.png'

const image = 'https://s3-alpha-sig.figma.com/img/cc4b/2826/12bf9db02e233aa64a34946d9a9aed4d?Expires=1713139200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=TCfwRUFH7x63R24S5bNPvXbFollwdcR~HmAuKu5FwBirkhO6FEBzTl1~KjGyxDvzQYQXhiVs2wI1K0sY2OME4IkxUCHsJzrFz0rU~ZcsXAjz-T08izmwe4Mq5837GT~85lc~9X88KaVQJgmykC1HsAAngxrQQtOO3YfY0Bqb~hWRPFo7vLD5eCADeeKH3pDXD6gv1ePq8drj1AtfhOrHWMMOdRlNQsSAvrEkxSPLhc27EOmJw3V08vz0N4LZ0PwQmHRn0Ik2bE0TYwH72c0U0AG9ziKVFQHJAoXcKgsf-FwIROPO3JQzoaXarVMHs3hvEUqJNTNJhlbPBa7qopNykw__';

const SmallBanner = (props) => {
    const type = props.type;

    return (
        <>
        {type === 'left'?
            <div className='wrap-SmallBanner-left'>
                <div className='rect-2'></div>
                <div className='rect-8'></div>
                <BannerArea 
                    type="leftOn"
                    title="Adoption"
                    text="We need help. so do they."
                    description="Adopt a pet and give it a home, it will be love you back unconditionally."/> 
                <div className='wrap-logo-left'>
                    <img  className="banner-logo-left" src={handOn} alt="img" />
                </div>
            </div>
        :
            <div className='wrap-SmallBanner'>
                <div className='rect-9'></div>
                <div className='rect-1'></div>
                <img  className="banner-logo" src={image} alt="img" />
                <BannerArea 
                    type="right"
                    title="One more friend"
                    text="Thousands more fun!"
                    description="Having a pet means you have more joy, a new friend, a happy person who will always be with you to have fun. We have 200+ different pets that can meet your needs!"/>
                <img  className="banner-logo banner-logo-mobile" src={image} alt="img" />
        
            </div>
        }
        </>
    );
};

export default SmallBanner;
