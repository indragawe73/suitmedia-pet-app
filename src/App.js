import React from 'react';
import { createBrowserHistory } from 'history';
import { HashRouter, Route, Routes } from 'react-router-dom';

import './App.css';
import Home from './pages/Home/Home';

const history = createBrowserHistory();

const App = () => {
  return (
      <HashRouter history={history}>
        <Routes>
            <Route path="/" element={<Home />} />
        </Routes>
      </HashRouter>
  );
}

export default App;