import React from 'react';
import './Home.css'
import BannerArea from '../../components/BannerArea/BannerArea'
import HeaderArea from '../../components/HeaderArea/HeaderArea'
import PetArea from '../../components/PetArea/PetArea'
import SmallBanner from '../../components/SmallBanner/SmallBanner'
import ButtonMain from '../../components/ButtonMain/ButtonMain'
import Footer from '../../components/Footer/Footer'

import Brands from '../../assets/images/Frame 40.png'

const MainLogo = 'https://s3-alpha-sig.figma.com/img/035a/16de/8e1aa9a0522d0cebb4144a5ceda0344a?Expires=1713139200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=PPmVlQj6FPajlKj9bIoL28NqmjeAFzL07CH6UEgiIhe2yfVvSyLfkiZq2DmagGCnGQbhVL3RlRXWy~hQ3PHB5QAIoT8vtGI4w2oKXFc5LDfZpmwTFln63bxCVGZfdBtB~F0R~aP2dXdPHHjREKv2QoHrQdlLPo-NtL7M4qCjVDW4bme2NbmFIqA2h2tqBOaQ86LrJzheS~rDVqRFc28Sv2TPXhVeD2EhIGjFJV6NTtgnzfsrr1yWYjPASY6GVxfY9JTHBjo1QdaJESqWyiSGwj38Nbxh7HAEk-lRFBSasv7cez1BVyFhY6x61j2SIBcbKI64z1ogyEeIoPSzFRI1Ag__';

const Home = () => {

    return (
        <div className='container'>
            <div className='herro-banner'>
                <div className='rectangle rectangle-1'></div>z
                <div className='rectangle rectangle-2'></div>
                <div className='rectangle rectangle-3'></div>
                <div className='rectangle rectangle-5'></div>
                <div className='rectangle rectangle-6'></div>
                <div className='rectangle rectangle-7'></div>
                <div className='rectangle rectangle-8'></div>
                <div className='rectangle rectangle-9'></div>
                <img  className="image-dog" src={MainLogo} alt="logo" />
                <HeaderArea />
                
                <div className='banner-area'>                
                    <BannerArea 
                        type="left"
                        title="One more friend"
                        text="Thousands more fun!"
                        description="Having a pet means you have more joy, a new friend, a happy person who will always be with you to have fun. We have 200+ different pets that can meet your needs!"/>
                </div>
            </div>
            
            <PetArea title="Whats new?" text="Take a look at some of our pets" />
            <SmallBanner />

            <div className='mobile-hide'>
                <PetArea title="Hard to choose right products for your pets?" text="Our Products" product="product" />

                <div className='wrap-seller'>
                    <div className='seller-area'>
                        <div className='seller-title'>
                            Proud to be part of <span>Pet Sellers</span>
                        </div>
                        <ButtonMain text='View all our sellers' level='tree'/>                
                    </div>
                    <img  className="image-brand" src={Brands} alt="Brands" />
                </div>

                <SmallBanner type="left"/>
            </div>

            <PetArea title="You already know ?" text="Useful pet knowledge" product="info" />
            <Footer />
        </div>
    );
};

export default Home;
