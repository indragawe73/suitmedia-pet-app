import Img01 from '../assets/images/Frame 7.png';
import Img02 from '../assets/images/image 2.png';
import Img03 from '../assets/images/image 3.png';
import Img04 from '../assets/images/image 4.png';
import Img05 from '../assets/images/image 5.png';
import Img06 from '../assets/images/image 6.png';
import Img07 from '../assets/images/image 7.png';
import Img08 from '../assets/images/image 8.png';

import Img11 from '../assets/images/Frame 11.png';
import Img12 from '../assets/images/Frame 12.png';
import Img13 from '../assets/images/Frame 13.png';
import Img14 from '../assets/images/Frame 14.png';
import Img15 from '../assets/images/Frame 15.png';
import Img16 from '../assets/images/Frame 16.png';
import Img17 from '../assets/images/Frame 17.png';
import Img18 from '../assets/images/Frame 18.png';

import Img21 from '../assets/images/Frame 21.png';
import Img22 from '../assets/images/Frame 22.png';
import Img23 from '../assets/images/Frame 23.png';

export const listPets = [
    { img: Img01, title : "MO231 - Pomeranian White", price : "6.900.000 VND", gender  : "Male", age : "02 months" },
    { img: Img02, title : "MO231 - Pomeranian White", price : "6.900.000 VND", gender  : "Male", age : "02 months" },
    { img: Img03, title : "MO231 - Pomeranian White", price : "6.900.000 VND", gender  : "Male", age : "02 months" },
    { img: Img04, title : "MO231 - Pomeranian White", price : "6.900.000 VND", gender  : "Male", age : "02 months" },
    { img: Img05, title : "MO231 - Pomeranian White", price : "6.900.000 VND", gender  : "Male", age : "02 months" },
    { img: Img06, title : "MO231 - Pomeranian White", price : "6.900.000 VND", gender  : "Male", age : "02 months" },
    { img: Img07, title : "MO231 - Pomeranian White", price : "6.900.000 VND", gender  : "Male", age : "02 months" },
    { img: Img08, title : "MO231 - Pomeranian White", price : "6.900.000 VND", gender  : "Male", age : "02 months" }
];


export const listProducts = [
    { img: Img11, title : "Reflex Plus Adult Cat Food Salmon", price : "140.000 VND", gender  : "Dog Food", age : "385gm" },
    { img: Img12, title : "Reflex Plus Adult Cat Food Salmon", price : "140.000 VND", gender  : "Dog Food", age : "385gm" },
    { img: Img13, title : "Reflex Plus Adult Cat Food Salmon", price : "140.000 VND", gender  : "Dog Food", age : "385gm" },
    { img: Img14, title : "Reflex Plus Adult Cat Food Salmon", price : "140.000 VND", gender  : "Dog Food", age : "385gm" },
    { img: Img15, title : "Reflex Plus Adult Cat Food Salmon", price : "140.000 VND", gender  : "Dog Food", age : "385gm" },
    { img: Img16, title : "Reflex Plus Adult Cat Food Salmon", price : "140.000 VND", gender  : "Dog Food", age : "385gm" },
    { img: Img17, title : "Reflex Plus Adult Cat Food Salmon", price : "140.000 VND", gender  : "Dog Food", age : "385gm" },
    { img: Img18, title : "Reflex Plus Adult Cat Food Salmon", price : "140.000 VND", gender  : "Dog Food", age : "385gm" }
];

export const listInfo = [
    { img: Img21, title : "Pet knowledge", price : "What is a Pomeranian? How to Identify Pomeranian Dogs", gender  : "The Pomeranian, also known as the Pomeranian (Pom dog), is always in the top of the cutest pets. Not only that, the small, lovely, smart, friendly, and skillful circus dog breed." },
    { img: Img22, title : "Pet knowledge", price : "What is a Pomeranian? How to Identify Pomeranian Dogs", gender  : "The Pomeranian, also known as the Pomeranian (Pom dog), is always in the top of the cutest pets. Not only that, the small, lovely, smart, friendly, and skillful circus dog breed." },
    { img: Img23, title : "Pet knowledge", price : "What is a Pomeranian? How to Identify Pomeranian Dogs", gender  : "The Pomeranian, also known as the Pomeranian (Pom dog), is always in the top of the cutest pets. Not only that, the small, lovely, smart, friendly, and skillful circus dog breed." }
];